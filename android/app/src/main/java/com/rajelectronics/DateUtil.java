package com.rajelectronics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

//    public static boolean isStartTimeGreater(Date startTime, Date endTime) {
//        boolean greater = false;
//
//        return greater;
//    }
//
//    public static boolean isEndTimeGreater(Date startTime, Date endTime) {
//        boolean greater = false;
//
//        return greater;
//
//    }
    public  static boolean isNowGreaterThan(String date)throws ParseException {
        //Now
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String now_text = sdf.format(new Date());
        Date then = sdf.parse(date);
        Date now = sdf.parse(now_text);

        long elapse = now.getTime() - then.getTime();
        return elapse > 0;


    }
}
