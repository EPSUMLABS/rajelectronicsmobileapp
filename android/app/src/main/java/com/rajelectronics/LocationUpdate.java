package com.rajelectronics;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.text.ParseException;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LocationUpdate extends Service {


    private LocationServiceBinder binder = new LocationServiceBinder();
    private LocationManager mLocationManager;
    public static final String TAG = "location-tracker";
    static String empid, starttime, endtime;
    private double latitude, longitude;
    private final int FOREGROUND_SERVICE_ID = 1262;
    private Context mContext = null;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private SharedPreferences preferences;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String NOTIFICATION_CHANNEL_ID = "com.rajelectronics.app";
            String channelName = "Tracking service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("rajelectronics is running")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
//            startForeground(2, notification);
            this.startForeground(FOREGROUND_SERVICE_ID, notification);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            String NOTIFICATION_CHANNEL_ID = "com.rajelectronics.app";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Raj Electronics is Tracking You")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            this.startForeground(FOREGROUND_SERVICE_ID, notification);
        }
        preferences = this.getSharedPreferences("LocationPrefs", MODE_PRIVATE);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Log.d(TAG, "onCreate");
    }


//    private void startMyOwnForeground() {
//        String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
//        String channelName = "My Background Service";
//        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
//        chan.setLightColor(Color.BLUE);
//        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        assert manager != null;
//        manager.createNotificationChannel(chan);
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
//        Notification notification = notificationBuilder.setOngoing(true)
//                .setSmallIcon(R.drawable.icon_1)
//                .setContentTitle("App is running in background")
//                .setPriority(NotificationManager.IMPORTANCE_MIN)
//                .setCategory(Notification.CATEGORY_SERVICE)
//                .build();
//        startForeground(2, notification);
//    }

//    private LocationListener mLocationListener = new LocationListener() {
//        @Override
//        public void onLocationChanged(Location location) {
//            Log.i(TAG, "onLocationChanged");
//            latitude = location.getLatitude();
//            longitude = location.getLongitude();
//            Log.i(TAG, "Latitude : " + latitude + "");
//            Log.i(TAG, "Longitude : " + longitude + "");
//            HttpHandler handler = new HttpHandler();
//            handler.execute(latitude + "", longitude + "", LocationUpdate.empid);
//        }
//
//        @Override
//        public void onStatusChanged(String s, int i, Bundle bundle) {
//            Log.i(TAG,"onStatusChanged");
//        }
//
//        @Override
//        public void onProviderEnabled(String s) {
//            Log.i(TAG,"onprovider Enabled");
//
//        }
//
//        @Override
//        public void onProviderDisabled(String s) {
//            Log.i(TAG,"on provider Disabled "+s);
//            if (mLocationManager != null) {
//                try {
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                            stopForeground(Service.STOP_FOREGROUND_REMOVE);
//                    }
//                } catch (Exception ex) {
//                    Log.i(TAG, "fail to remove location listners, ignore", ex);
//                }
//            }
//        }
//    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);
        Bundle bundle = intent.getExtras();
        LocationUpdate.empid = bundle.getString("empid");
        LocationUpdate.starttime = bundle.getString("starttime");
        LocationUpdate.endtime = bundle.getString("endtime");
        Log.d(TAG, "onStartCommand: " + empid + " - " + starttime + " - " + endtime);

//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.mContext = this;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();

        } else {
            Log.d(TAG, "onStartCommand: request location updates invoked");
//            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, mLocationListener);
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(180 * 1000);

            locationCallback = new LocationCallback() {


                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        Log.d(TAG, "onLocationResult: " + "The location is null");
                        return;
                    }
                    try {
                        if (DateUtil.isNowGreaterThan(LocationUpdate.starttime) && (!DateUtil.isNowGreaterThan(LocationUpdate.endtime))) {
                            Log.d(TAG,"Within time range");
                            for (Location location : locationResult.getLocations()) {
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();

                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("latitude", latitude + "");
                                    editor.putString("longitude", longitude + "");
                                    editor.apply();

                                    Log.d(TAG, "onLocationResult: Latitude - " + location.getLatitude() + " Longitude - " + location.getLongitude());
                                    HttpHandler handler = new HttpHandler();
                                    handler.execute(latitude + "", longitude + "", LocationUpdate.empid);

                                }
                            }
                        }else{
                            // Sit around do nothing
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("latitude",null);
                            editor.putString("longitude",null);
                            editor.apply();
                            Log.e(TAG,"Office hour khatam");


                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        if (mLocationManager != null) {
//            try {
//                mLocationManager.removeUpdates(mLocationListener);
//            } catch (Exception ex) {
//                Log.i(TAG, "fail to remove location listners, ignore", ex);
//            }
//        }

//        mLocationManager.removeUpdates(mLocationListener);
        mFusedLocationClient.removeLocationUpdates(locationCallback);
        Log.d(TAG, "onDestroy: " + "Service destroyed");
    }

    private static class HttpHandler extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final MediaType JSON
                    = MediaType.parse("application/json; charset=utf-8");
            String url = "http://69.46.104.81/webapimvc/api/Employee/Location";

            String json = "{\n" +
                    "\t\"employeeid\":\"" + params[2] + "\",\n" +
                    "\t\"coordinate\":\"[{\\\"lat\\\":" + params[0] + ",\\\"lon\\\":" + params[1] + ",\\\"empid\\\":\\\"" + params[2] + "\\\"}]\"\n" +
                    "}";
            OkHttpClient client = new OkHttpClient();


            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            try (Response response = client.newCall(request).execute()) {
                try {
                    return response.body().string();
                } catch (NullPointerException npe) {
                    npe.printStackTrace();
                    Log.e("Location Service", "onLocationChanged: " +
                            npe.toString());
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            try {
                Log.e("location-tracker", "onPostExecute: " + response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public class LocationServiceBinder extends Binder {
        public LocationUpdate getService() {
            return LocationUpdate.this;
        }
    }


}
