package com.rajelectronics;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

import static android.content.Context.MODE_PRIVATE;

public class ConfigureTracking extends ReactContextBaseJavaModule {


    public ConfigureTracking(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nonnull
    @Override
    public String getName() {
        return "ConfigureTracking";
    }

    @ReactMethod
    public void configure(String empid, String starttime, String endtime) {
        try {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getReactApplicationContext());
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("empid", empid);
            editor.putString("starttime", starttime);
            editor.putString("endtime", endtime);
            editor.apply();

            Intent locationservice = new Intent(getCurrentActivity(), LocationUpdate.class);
            Bundle info = new Bundle();
            info.putString("empid", empid);
            info.putString("starttime", starttime);
            info.putString("endtime", endtime);
            locationservice.putExtras(info);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getCurrentActivity().startForegroundService(locationservice);
            } else {
                getCurrentActivity().startService(locationservice);

            }

        } catch (NullPointerException npe) {
            Toast.makeText(getReactApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void logout() {
        getReactApplicationContext().stopService(new Intent(getCurrentActivity(), LocationUpdate.class));


    }

    @ReactMethod
    public void getLastKnownLocation(Callback getLocation, Callback error) {
        LocationManager manager = (LocationManager)getReactApplicationContext().getSystemService(getReactApplicationContext().LOCATION_SERVICE );
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(statusOfGPS) {
            SharedPreferences preferences = getReactApplicationContext().getSharedPreferences("LocationPrefs", MODE_PRIVATE);
//        SharedPreferences.Editor editor = preferences.edit();
            String latitude = preferences.getString("latitude", null);
            String longitude = preferences.getString("longitude", null);
            Log.i(LocationUpdate.TAG, "Latitude - " + latitude + " Longitude - " + longitude);
            if (latitude == null || longitude == null) {
                error.invoke("Error occurred while fetching GPS location.. Your GPS is probably off. Please turn on the GPS and try again");
            } else {
                getLocation.invoke(latitude, longitude);
            }
        }else{
            error.invoke("Error occurred while fetching GPS location.. Your GPS is probably off. Please turn on the GPS and try again");

        }

    }
}
