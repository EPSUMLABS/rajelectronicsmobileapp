
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage, Alert} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Picker,List,ListItem,
  Textarea,Card,CardItem,Radio} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import StarRating from 'react-native-star-rating';
import urldetails from '../config/endpoints.json';
export default class Feedbackform extends Component{
    state={
    open:true,
    starCount: 3,
    questionlist:[],
    review:'',
    employeeid:'',
    mobileno:this.props.navigation.state.params.mobileno,
    invoiceno:this.props.navigation.state.params.invoiceno
    }
    componentWillMount(){
      AsyncStorage.getItem("user")
    .then((value) => {
      const values = JSON.parse(value)
      this.setState({employeeid:values.employeeid})
    });
      this.getquestions()
      }
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
       this.props.navigation.navigate("Customerfeedback")
       return true;
   
      };
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });

      modalDidOpen1 = () => console.log("Modal did open.");
      modalDidClose1 = () => {
        this.setState({ open1: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal1 = () => this.setState({ open1: true });
      closeModal1 = () => this.setState({ open1: false });

      onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
      getquestions(){
        fetch(urldetails.base_url+urldetails.url["feedbackquestion"],{
          method: "POST",
          headers: {
             Accept: 'application/json',
            'Content-Type':'application/json',
          },
        })
      .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        if(jsondata.status=="Success"){
          this.setState({questionlist:jsondata.Message,open:false})
        }
        else{
          this.setState({open:false})
          alert(jsondata.Message)
        } 
      }).catch((error) => {
        this.setState({open:false})
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
          [
            {text: 'Retry', onPress: () =>this.getquestions()},
          ],
          { cancelable: false }
        )
        });  
    }
    customerfeedback(){
      this.setState({open:true})
      let x = JSON.stringify({"clientMobNo":this.state.mobileno, "invoiceno":this.state.invoiceno,
      "quesid":this.state.questionid, "ans":this.state.starCount, "remark":this.state.review,
      "salespersonname":this.state.employeeid
      });;
      console.log(x)
      fetch(urldetails.base_url+urldetails.url["customerfeedback"],{
        method: "POST",
        headers: {Accept: 'application/json', 'Content-Type':'application/json'},
        body:x
      })
    .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="Success"){
        this.setState({open:false,open1:false})
        Alert.alert(
          'Alert',
          jsondata.Message,
          [
            {text: 'OK', onPress: () =>this.props.navigation.navigate('Home')},
          ],
          { cancelable: false }
        )
        
      }
      else{
        alert(jsondata.Message)
      }  
    })
    }
  
  render() {
    return (
      <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
       <Left/>
          <Body><Title>Customer Feedback</Title></Body>
          </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content >
        <View>
          {this.state.questionlist.map((item,i)=>{
            return(
        <ListItem  onPress={()=>this.setState({open1:true,questionid:item.quesid})} key={i}>
            <Text style={{paddingLeft:3}}>{item.ques}</Text>  
        </ListItem>
            )}
          )}  
         </View>        
       </Content>
       {/* <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#42378F','#F53844']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity  style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>Submit</Text>
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer> */}
       <Modal offset={0} open={this.state.open} modalDidOpen={this.modalDidOpen} modalDidClose={this.modalDidClose}
          style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
          <View style={{alignContent:'center',alignItems:'center'}}>
          <Spinner color='#F53844' />
          </View>         
          </Modal>
          <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1}
            modalDidClose={this.modalDidClose1}>          
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ backgroundColor: 'transparent', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
        <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.state.starCount}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
        fullStarColor={'#FFD700'}/>                    
        </View>
         <View style={{padding:10}}>
          <Text>Is there anything else you would like us to know about your experience?</Text>
          <Textarea style={{ width:screenwidth-80,marginTop:10,textAlign: 'center',borderColor:'#df3a45',borderWidth:0.5}} rowSpan={7} bordered placeholder="Write Your Review Here"
           onChangeText={(rev) => this.setState({ review: rev })} />
          </View>                
        {/* <Item regular style={{width:'95%',alignItems:'center',backgroundColor:'#fff',borderRadius:5,marginTop:20,textAlign:'center',justifyContent:'center'}}>
          <Input style={{fontSize:16,color:'black'}} placeholder='Your Name'  onChangeText={(name)=>this.setState({name:name})} autoCorrect={false} />
        </Item> */}
                       
                        <TouchableOpacity onPress={() => this.customerfeedback()} style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            backgroundColor:'#df3a45',alignItems: "center", justifyContent: 'center',}}>
                            <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>
                        </TouchableOpacity>
                       
                    </View>
                </Modal1>

       </Container>
    );
  }
}
const styles = {
preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}
