//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Spinner} from 'native-base';

// create a component
class Redirect extends Component {
    componentWillMount(){
        this.props.navigation.navigate('Splash')
    }
    componentDidMount(){
        this.props.navigation.navigate('Login')
    }
    render() {
        return (
            <View style={styles.container}>
                <Text>Redirect</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000',
    },
});

//make this component available to the app
export default Redirect;