//import liraries
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, StatusBar, AsyncStorage, BackHandler, StyleSheet, Picker } from 'react-native';
import { Container,Content, Header, Title, Left, Body, Right, Icon, List,ListItem, Spinner} from 'native-base';
import urldetails from '../config/endpoints.json';
import Modal from "react-native-simple-modal";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

// create a component
class Emp_Attendance extends Component {

    state={
        data:[],
        open:false,
        employeeid:'',
        month:0,
        year:''
    }

    modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
  };
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }

    componentWillMount(){
        AsyncStorage.getItem("user")
    .then((value) => {
      const values = JSON.parse(value)
      var nav = "";
      console.log(values.role)
      this.setState({employeeid:values.employeeid});
    })
    }

    fetch_attendance(){
        this.setState({open:true})
        let req_data = JSON.stringify({"month":this.state.month,"year":this.state.year,"employeeid":this.state.employeeid});
        console.log(req_data);
        fetch(urldetails.base_url+urldetails.url["emp_attendance"],{ method: "POST",
          headers: { Accept: 'application/json', 'Content-Type':'application/json'},
          body:req_data
        }) .then((response)=>response.json())
        .then((jsondata)=>{
            console.log(jsondata)
            if(jsondata.data.length>0){
                this.setState({data:jsondata.data,open:false});
            }
            else{
                this.setState({open:false});
                alert('No Record found...');
            }
        });
    }

    _renderyear(){
        let views=[];
         var today = new Date();
          views.push( <Picker.Item label={String(today.getFullYear())} value={String(today.getFullYear())} key={1} /> );
          views.push( <Picker.Item label={String(today.getFullYear()-1)} value={String(today.getFullYear()-1)} key={2} /> );
        console.log(String(today.getFullYear()))
          return views;
      }

      
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#df3a45' }}>
                    <Left>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity> */}
                    </Left>
                    <Body><Title>Attendance</Title></Body>
                    <Right />
                </Header>
                <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
                <Content style={{ backgroundColor: '#fff' }}>
                    <View style={{ flexDirection: "row", alignContent: 'center', justifyContent: 'center' }}>
                        <Picker
                            selectedValue={this.state.month}
                            style={{ height: 50, color: '#000', width: screenWidth / 2 - 15, }}
                            onValueChange={(itemValue, itemIndex) => { this.setState({ month: itemValue }) }}>
                            <Picker.Item label={"Month"} value={0} key={0} />
                            <Picker.Item label={"January"} value={1} key={1} />
                            <Picker.Item label={"Febuary"} value={2} key={2} />
                            <Picker.Item label={"March"} value={3} key={3} />
                            <Picker.Item label={"April"} value={4} key={4} />
                            <Picker.Item label={"May"} value={5} key={5} />
                            <Picker.Item label={"June"} value={6} key={6} />
                            <Picker.Item label={"July"} value={7} key={7} />
                            <Picker.Item label={"August"} value={8} key={8} />
                            <Picker.Item label={"September"} value={9} key={9} />
                            <Picker.Item label={"October"} value={10} key={10} />
                            <Picker.Item label={"November"} value={11} key={11} />
                            <Picker.Item label={"December"} value={12} key={12} />
                        </Picker>

                        <Picker
                            selectedValue={this.state.year}
                            style={{ height: 50, color: '#000', width: screenWidth / 2 - 15, }}
                            onValueChange={(itemValue, itemIndex) => { this.setState({ year: itemValue }) }}>
                            <Picker.Item label={"Year"} value={0} key={0} />
                            {this._renderyear()}
                            
                            
                        </Picker>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={()=>this.fetch_attendance()} style={{
                            height: 40, width: screenWidth - 40, backgroundColor: '#df3a45',
                            borderRadius: 5, alignContent: 'center', alignItems: 'center', justifyContent: 'center'
                        }}>
                            <Text style={{ fontSize: 20, color: '#fff' }}>SEARCH</Text>
                        </TouchableOpacity>
                    </View>

                    <List>
                        <ListItem>
                            <Text style={{ paddingRight: screenWidth / 4 }}>Day</Text>
                            <Text style={{ paddingRight: screenWidth / 4 }}>Date</Text>
                            <Text style={{ paddingRight: screenWidth / 4 }}>status</Text>
                        </ListItem>
                    </List>
                    <List dataArray={this.state.data}
                        renderRow={(item) =>
                            <ListItem>
                                <Text style={{ paddingRight: screenWidth / 4 }}>{item.day}</Text>
                                <Text style={{ paddingRight: screenWidth / 4 }}>{item.date.substr(0, 10)}</Text>
                                {item.status == 'P' ?
                                    <Text style={{ paddingRight: screenWidth / 4, color: '#27AE60', fontSize: 16, fontWeight: 'bold' }}>{item.status}</Text> :
                                    <Text style={{ paddingRight: screenWidth / 4, color: 'red', fontSize: 16, fontWeight: 'bold' }}>{item.status}</Text>}
                            </ListItem>}>
                    </List>
                </Content>
                <Modal offset={0} open={this.state.open} modalDidOpen={this.modalDidOpen} disableOnBackPress={true}
                    closeOnTouchOutside={false} modalDidClose={this.modalDidClose} style={{ alignItems: "center" }}>
                    <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center', marginTop: 10 }}>
                        <Spinner color='#df3a45' />
                        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>Please wait...</Text>
                    </View>
                </Modal>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Emp_Attendance;
