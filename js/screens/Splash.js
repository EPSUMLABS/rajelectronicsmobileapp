
import React, {Component} from 'react';
import {AppState,
    View,Platform,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,
  } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import ConfigureTracking from '../modules/ConfigureTracking';
  const screenWidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;
export default class Splash extends Component{
 
  componentWillMount(){
    AsyncStorage.getItem("user")
    .then((value) => {
      const values = JSON.parse(value)
      if(values!=null || values!=undefined || values=="")
      {
        setTimeout(()=>this.props.navigation.navigate("Home"),1000)
        ConfigureTracking.configure(values.employeeid, values.track_start, values.track_end)
      }
      else{
        setTimeout(()=>this.props.navigation.navigate("Login"),1000)
      }
    })
    }

  render() {
    return (
      <Container>
      <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content >
      <LinearGradient colors={['#F53844','#42378F']} style={{ flex: 1, alignItems: 'center',height:screenHeight-15 }}>
       <View style={{ flex: 1, alignItems: 'center' }}>
           <View style={{alignItems: 'center', justifyContent: 'center',marginTop:100}}>
            <Text style={{marginTop:'30%',fontSize:35,color:'#fff',fontFamily: 'Italic',}}>Raj</Text>
            <Text style={{paddingTop:5,fontSize:35,color:'#fff'}}>Electronics</Text>
               <Spinner color='#fff' style={{paddingTop:30}}/>
           </View>
       </View>
      </LinearGradient>
       </Content>
       
       </Container>
    );
  }
}

