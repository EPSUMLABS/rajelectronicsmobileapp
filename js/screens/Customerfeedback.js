
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,ScrollView,List,ListItem,
    Card,CardItem} from 'native-base';
import Modal from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
export default class Splash extends Component{
    state={
    open:false,
    invoiceno:'',
    itemlist:[]
    }
    
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });

      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home")
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }

      getinvoicewiseitems(){
        this.setState({open:true})
        //ELPL/1819/12
        let x = JSON.stringify({"invoiceno":this.state.invoiceno});;
        console.log(x)
        fetch(urldetails.base_url+urldetails.url["invoicewiseitems"],{
          method: "POST",
          headers: {
             Accept: 'application/json',
            'Content-Type':'application/json',
          },
          body:x
        })
      .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)  
        if(jsondata.status=="Success"){
          this.setState({itemlist:jsondata.Message,open:false})
          console.log(this.state.itemlist)
        }
        else{
         this.setState({open:false})
         alert(jsondata.Message)
        }
      }).catch((error) => {
        this.setState({open:false})
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
          [
            {text: 'Retry', onPress: () =>this.getinvoicewiseitems()},
          ],
          { cancelable: false }
        )
        });  
      }
      handle_feedback(){
        if(this.state.itemlist.length>0){
          this.props.navigation.navigate("Feedbackform",{"mobileno":this.state.itemlist[0].mobileno, "invoiceno":this.state.invoiceno})
        }else{
          alert('Please Select a valid Invoice to continue.')
        }
      }
  render() {
    return (
      <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
      
          <Body><Title style={{width:screenwidth}}>Customer Feedback</Title></Body>
          <Right/>
          </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content >
        <View style={{alignContent:'center',justifyContent:'center',alignItems:'center',marginTop:10}}>
        <View style={{marginTop:10}}>
          <View style={{flexDirection:'row',backgroundColor:'#EBEBEB',width:screenwidth-20,height:screenHeight/16,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Input style={{fontSize:16, paddingLeft:10}} onChangeText={(invoiceno)=>this.setState({invoiceno:invoiceno})} placeholder='Enter Invoice number' placeholderTextColor='#000' />
            </View>
         </View>
         <TouchableOpacity onPress={()=>this.getinvoicewiseitems()} 
         style={{width:screenwidth-20,margin:20,height:50,borderRadius:10,backgroundColor:'#F53844',alignContent:'center',alignItems:'center',justifyContent:'center'}}>
        <Text style={{fontSize:15,color:'#fff'}}>Search</Text>
         </TouchableOpacity>
         </View>
         <View style={{alignContent:'center',alignItems:'center'}}>
         {this.state.itemlist.map((item,i)=>{
           return(
         <Card style={{width:screenwidth-20}} key={i}>
               <CardItem style={{flexDirection:'row',backgroundColor:'#EBEBEB',alignContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{flexDirection:'row',paddingLeft:2, alignContent:'center', justifyContent:'center'}} onPress={()=>this.setState({open2:true})} >
               <Left>
                <Text style={{color:'#000',paddingRight:5 }}>Item Name : </Text>
                </Left>
                <Right style={{flexDirection:'row'}}>
                <Text style={{ fontSize:15,paddingLeft:5}}>{item.itemname}</Text>
                </Right>
               </TouchableOpacity>
                  </CardItem>
                    <CardItem>
                    <Text style={{ fontSize: 16,color:'#000' }}>Amount : </Text>
                    <Text style={{ fontSize: 16 }}>{item.totaltaxamount}</Text>
                    </CardItem>
                  </Card> 
           )}
         )}      
         </View>        
       </Content>
       <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#42378F','#F53844']} 
           start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>this.handle_feedback()}
             style={{width:screenwidth,flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>Feedback</Text>
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer>
       <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
          <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#F53844' />
              </View>         
          </Modal>
       </Container>
    );
  }
}
const styles = {
preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}
