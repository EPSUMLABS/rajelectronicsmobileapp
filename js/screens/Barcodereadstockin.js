import React, { Component } from 'react';
import { TouchableOpacity, Text, View, BackHandler, AsyncStorage, Alert } from 'react-native';

import {PermissionsAndroid} from 'react-native';
import BarcodeScanner, { Exception, FocusMode, CameraFillMode, FlashMode, BarcodeType, pauseScanner, resumeScanner } from 'react-native-barcode-scanner-google';
import { Toast } from 'native-base';

export default class Barcodereadstockin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemname: this.props.navigation.state.params.itemname,
      itemlist:this.props.navigation.state.params.itemlist,
      barcode:'',
    };
  }
  componentWillMount(){
    if(this.requestCameraPermission()){

    }else{
      alert("Camera Permission denied. Please grant Camera Permission")
        this.props.navigation.navigate('Home');
    }
  }
  requestCameraPermission() {
    if (PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA)){
      return true
    }else {
      return false;
    }
    
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Addstock",{itemlist:this.state.itemlist,itemname:this.state.itemname,barcode:""})
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }

  onReadComplete(scanResult) {
    // console.warn(scanResult.type);
    // console.warn(scanResult.data);
    console.log(scanResult)
    this.props.navigation.navigate("Addstock",{itemlist:this.state.itemlist, itemname:this.state.itemname,barcode:scanResult})
    // this.props.navigation.navigate("Packagedetails")
     
  }
  
  render() {
    return (
      <View style={styles.container}>
      <BarcodeScanner
            style={{flex: 1}}
            onBarcodeRead={({data, type}) => {this.onReadComplete(data)}}
            onException={exceptionKey => {
                // check instructions on Github for a more detailed overview of these exceptions.
                switch (exceptionKey) {
                    case Exception.NO_PLAY_SERVICES:
                        // tell the user they need to update Google Play Services
                    case Exception.LOW_STORAGE:
                        // tell the user their device doesn't have enough storage to fit the barcode scanning magic
                    case Exception.NOT_OPERATIONAL:
                        // Google's barcode magic is being downloaded, but is not yet operational.
                    default: break;
                }
            }}
            focusMode={FocusMode.AUTO /* could also be TAP or FIXED */}
            cameraFillMode={CameraFillMode.COVER /* could also be FIT */}
            barcodeType={BarcodeType.ALL /* replace with ALL for all alternatives */}
            FlashMode={FlashMode.OFF /* 0 is OFF or 1 is TORCH  */}
        />
      <View style={[styles.overlay, styles.topOverlay]}>
  <Text style={styles.scanScreenMessage}>Please scan the barcode.</Text>
</View>
    </View>

    );
  }
}
const styles = {
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  enterBarcodeManualButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40
  },
  scanScreenMessage: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  }
};

