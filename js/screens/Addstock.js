
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage, Alert} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Picker,List,ListItem,Radio} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import ColorPalette from 'react-native-color-palette';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
export default class Addstock extends Component {
  constructor(props) {
    super(props);
    this.state={
      itemname: this.props.navigation.state.params.itemname,
      type:'',open:false,open1:false,
      itemlist:this.props.navigation.state.params.itemlist,
      itemid:'',quanity:'',
      barcode:this.props.navigation.state.params.barcode,
      supplierlist:[],suppliername:'',supplierid:'',
      categorylist:[],categoryname:'',categoryid:'',
      brandlist:[],brandname:'',
      storelist:[],storename:'',storeid:'',
      warehouselist:[],warehousename:'',wid:'',
      itemdata:[],
      hsncode:'',
      itemdesc:'',
      unit:'',purchaseprice:'',sellingprice:'',mrp:'',
      employeeid:''
    }
  }
  componentWillMount(){
    AsyncStorage.getItem("user")
    .then((value) => {
      const values = JSON.parse(value)
      this.setState({employeeid:values.employeeid})
    });
   this.filter_item(this.state.itemname)
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Selectitem")
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });

  modalDidOpen1 = () => console.log("Modal did open.");
  modalDidClose1 = () => {
    this.setState({ open1: false });
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal1 = () => this.setState({ open1: true });
  closeModal1 = () => this.setState({ open1: false });
  filter_item(item) {
    this.setState({view:true,open:false,itemdata:[0],newitemlist:this.state.itemlist})
    var arr = []
    this.state.itemlist.map((myitem, i) => {
      if (myitem.itemname === item) {
        console.log(myitem.itemname)
        arr.push(myitem)
        console.log(arr)
        this.setState({itemdata:arr[0],itemname:arr[0].itemname,itemid:arr[0].itemid,hsncode:arr[0].hsncode,itemdesc:arr[0].itemdesc,
        unit:arr[0].unit,purchaseprice:arr[0].purchaseprice,sellingprice:arr[0].sellingprice,mrp:arr[0].mrp})
        console.log(this.state.itemdata)
      }
    }) 
  }
  searchitem(myitem){
    const newData = this.state.itemlist.filter((mydata)=>{
      const itemData = mydata.itemname.toUpperCase()
      console.log(itemData)
      const textData = myitem.toUpperCase()
      console.log(textData)
      return itemData.indexOf(textData)>-1
    });
    
    this.setState({myitem:myitem,newitemlist:newData});
  
  console.log(this.state.newitemlist)
  }
  getsupplier(){
    this.setState({open1:true})
    let x = JSON.stringify({"agid":31});;
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["suppliers"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Sucess"){
      console.log(jsondata)
      this.setState({supplierlist:jsondata.data.supplier,open1:false,open:true,type:"supplier"})
    }
  }).catch((error) => {
    this.setState({open:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getsupplier()},
      ],
      { cancelable: false }
    )
    });  
  }
  getcategory(){
    this.setState({open1:true})
    let x = JSON.stringify({"action":"fetch"});;
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["itemcategory"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Sucess"){
      console.log(jsondata)
      this.setState({categorylist:jsondata.data.supplier,open1:false,open:true,type:"category"})
    }
  }).catch((error) => {
    this.setState({open:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getcategory()},
      ],
      { cancelable: false }
    )
    });  
  }
  getbrand(){
    this.setState({open1:true})
    let x = JSON.stringify({"action":"fetch"});;
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["brand"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Sucess"){
      console.log(jsondata)
      this.setState({brandlist:jsondata.data.supplier,open1:false,open:true,type:"brand"})
    }
  }).catch((error) => {
    this.setState({open:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getbrand()},
      ],
      { cancelable: false }
    )
    });  
  }
  getstore(){
    this.setState({open1:true})
    let x = JSON.stringify({"action":"fetch"});;
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["store"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Sucess"){
      console.log(jsondata)
      this.setState({storelist:jsondata.data.supplier,open1:false,open:true,type:"store"})
    }
  }).catch((error) => {
    this.setState({open:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getstore()},
      ],
      { cancelable: false }
    )
    });  
  }
  getwarehouse(){
    this.setState({open1:true})
    let x = JSON.stringify({"action":"fetch"});
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["warehouse"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    console.log(jsondata.data.supplier)  
    if(jsondata.status=="Sucess"){
      this.setState({warehouselist:jsondata.data.supplier,open1:false,open:true,type:"warehouse"})
    }
  }).catch((error) => {
    this.setState({open:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getwarehouse()},
      ],
      { cancelable: false }
    )
    });  
  }

 openmodal(){
   if(this.state.type=="item")
   {
   return(
     <View >
    <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
    
    <Item regular style={{height:40,width:screenwidth-100,alignItems:'center',borderRadius:5,marginLeft:5,borderColor: '#000'}}>
          <Input style={{fontSize:16,color:'#000'}} placeholder='Search'placeholderTextColor='#000' onChangeText={(itemname)=>this.searchitem(itemname)} autoCapitalize="none" autoCorrect={false} />
          </Item>
    </View>
    <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
    <List dataArray={this.state.newitemlist}
                  renderRow={(myitem) => 
            <ListItem onPress={()=>this.filter_item(myitem.itemname)}>
              <Text>{myitem.itemname}</Text>
             </ListItem>
      }>
     </List>
    </View>
    </View>
   )
    }
    else if(this.state.type=="supplier"){
      return(
        <View >
       <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
       <Text>SELECT SUPPLIER</Text>
       </View>
       <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
       <List dataArray={this.state.supplierlist}
                     renderRow={(myitem) => 
               <ListItem onPress={()=>this.setState({open:false,suppliername:myitem.accountheadname,supplierid:myitem.accountheadid})}>
                 <Text>{myitem.accountheadname}</Text>
                </ListItem>
         }>
        </List>
       </View>
       </View>
      )
    }
    else if(this.state.type=="category"){
      return(
        <View >
       <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
       <Text>SELECT CATEGORY</Text>
       </View>
       <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
       <List dataArray={this.state.categorylist}
                     renderRow={(myitem) => 
               <ListItem onPress={()=>this.setState({open:false,categoryname:myitem.catagoryname,categoryid:myitem.catagoryid})}>
                 <Text>{myitem.catagoryname}</Text>
                </ListItem>
         }>
        </List>
       </View>
       </View>
      )
    }
    else if(this.state.type=="brand"){
      return(
        <View >
       <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
       <Text>SELECT Brand</Text>
       </View>
       <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
       <List dataArray={this.state.brandlist}
                     renderRow={(myitem) => 
               <ListItem onPress={()=>this.setState({open:false,brandname:myitem.brandname})}>
                 <Text>{myitem.brandname}</Text>
                </ListItem>
         }>
        </List>
       </View>
       </View>
      )
    }
    else if(this.state.type=="store"){
      return(
        <View >
       <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
       <Text>SELECT STORE</Text>
       </View>
       <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
       <List dataArray={this.state.storelist} renderRow={(myitem) => 
        <ListItem onPress={()=>this.setState({open:false,storename:myitem.storename,storeid:myitem.storeid})}>
        <Text>{myitem.storename}</Text>
        </ListItem>
         }>
        </List>
       </View>
       </View>
      )
    }
    else if(this.state.type=="warehouse"){
      
      return(
        <View >
       <View style={{alignContent:'center',alignItems:'center',padding:5,width:screenwidth/1.5,flexDirection:'row'}}>
       <Text>SELECT WAREHOUSE</Text>
       </View>
       <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
       <List dataArray={this.state.warehouselist} renderRow={(myitem) => 
        <ListItem onPress={()=>this.setState({open:false, warehousename:myitem.storename, wid:myitem.storeid})}>
        <Text>{myitem.storename}</Text>
        </ListItem>
         }>
        </List>
       </View>
       </View>
      )
    }
    else if(this.state.type=="color"){
      let selectedColor = '#C0392B';
      return(
    <View style={{height:screenHeight}}>
    <ColorPalette 
    onChange={color =>this.setState({color:color,open:false})}
    defaultColor={'#C0392B'}
    colors={['#F0F8FF','#FAEBD7','#00FFFF','#7FFFD4','#F0FFFF','#F5F5DC','#FFE4C4','#000000','#FFEBCD','#0000FF','#8A2BE2','#A52A2A','#DEB887','#5F9EA0','#7FFF00',
  '#D2691E','#FF7F50','#6495ED','#DC143C']}
    icon={
      <Text >✔</Text>
      // Icon can just be text or ASCII
    }
  />
  </View>
      )
    }
 }
submit(){
  this.setState({open1:true})
  var storeid = ""; 
  if(this.state.storeid!="" && this.state.wid!=""){
    this.setState({open1:false})
    Alert.alert(
      'Alert',
      "You can select only one from wharehour and store.",
      [
        {text: 'ok', onPress: () =>this.props.navigation.navigate("Home")},
      ],
      { cancelable: false }
    )
  }
  else if(this.state.storeid!=""){
    storeid=this.state.storeid
  }
  else{
    storeid = this.state.wid
  }
  if(storeid!=""){
  let x = JSON.stringify({"Itemid":this.state.itemid,"noofItem":this.state.quanity,"storeid":storeid,
  "suppliersid":this.state.supplierid,"barcode":this.props.navigation.state.params.barcode,"description":this.state.itemdesc,"itemgroup":'',
"hsncode":this.state.hsncode,"colorname":this.state.color,"deleted":'0',"createdon":'', "createdby":this.state.employeeid});;
    console.log(x)
    fetch('http://69.46.104.81/webapimvc/api/Stock/StockSubmit',{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    if(jsondata.status=="Sucess"){
      this.setState({open1:false})
    alert(jsondata.Message)
    this.props.navigation.navigate("Home");
    }
    else{
      this.setState({open1:false})
      alert(jsondata.Message) 
    }
  }).catch((error) => {
    this.setState({open1:false})
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.submit()},
      ],
      { cancelable: false }
    )
    });  
}
}
  render() {
    
    
    return (
      <Container>
          <Header style={{ backgroundColor: '#F53844' }}>
          <Left/>
          <Body><Title>Add Stock</Title></Body>
          <Right/>
          </Header>
          <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
          <Content style={{backgroundColor:'#F0F3F4'}}>
          {this.state.view==true?
          <View style={{alignItems:'center',alignContent:'center',justifyContent:'center'}}>
          <TouchableOpacity onPress={()=>this.setState({open:true,type:"item",barcode:''})} style={{flexDirection:'row',margin:10}}>
          <Text style={{fontWeight:'bold',fontSize:15}}>Item Name : </Text>
          <Text style={{fontSize:15}}>{this.state.itemname}</Text> 
          </TouchableOpacity>
          <View style={{backgroundColor:'#fff',width:screenwidth,paddingTop:5}}>
          <List>
            <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Hsn Code : </Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.hsncode}</Text>
            </ListItem> 
            {/* <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Item Group :</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>software application</Text>
            </ListItem>  */}
            <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Description:</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.itemdesc}</Text>
            </ListItem>             
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.props.navigation.navigate("Barcodereadstockin",{itemlist:this.state.itemlist,itemname:this.state.itemname})}>
            <Left><Text style={{color:'#000',fontSize:15}}>Bar Code :</Text></Left>
            <Body>
            <Text style={{fontSize:15}}>{this.props.navigation.state.params.barcode}</Text>
            </Body>
            <Right>
             <AntDesign name='pluscircle' style={{color:'#F53844',fontSize:20}}/>
             </Right> 
            </ListItem>          
             </List>
           </View>         
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem>
           <Text style={{color:'#000',fontSize:15}}>Unit :</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.unit}</Text>
            </ListItem> 
            <ListItem>
            <View style={{flexDirection:'row'}}> 
            <Text style={{color:'#000',fontSize:15}}>Purchase Price :</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.purchaseprice}</Text>
            </View>
            </ListItem>
             <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Sale Price:</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.sellingprice}</Text>
            </ListItem>  
             <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Mrp:</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>{this.state.mrp}</Text>
            </ListItem> 
{/*            
            <ListItem>
            <Text style={{color:'#000',fontSize:15}}>Nos Of Stock :</Text>
            <Text style={{fontSize:15,paddingLeft:5}}>100</Text>
            </ListItem>            */}
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.getsupplier()}>
              <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Supplier:</Text>
           </Left>
           <Text style={{fontSize:15,paddingLeft:5}}>{this.state.suppliername.substr(0,30)}</Text>
           <Right><AntDesign name="caretdown"
                 style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>          
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.getcategory()}>
              <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Category:</Text>
           </Left>
           <Text style={{fontSize:15,paddingLeft:5}}>{this.state.categoryname.substr(0,30)}</Text>
           <Right><AntDesign name="caretdown"
                 style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>          
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.getbrand()}>
              <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Brand:</Text>
           </Left>
           <Text style={{fontSize:15,paddingLeft:5}}>{this.state.brandname}</Text>
           <Right><AntDesign name="caretdown"
                 style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>          
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.getstore()}>
              <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Store:</Text>
           </Left>
           <Text style={{fontSize:15,paddingLeft:5}}>{this.state.storename}</Text>
           <Right><AntDesign name="caretdown"
                 style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>          
             </List>
             <List>
            <ListItem onPress={()=>this.getwarehouse()}>
              <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Warehouse:</Text>
           </Left>
           <Text style={{fontSize:15,paddingLeft:5}}>{this.state.warehousename}</Text>
           <Right><AntDesign name="caretdown" style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>          
             </List>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
            <Body>
              <Input placeholder='Enter Number Of Items' onChangeText={(noofitem)=>this.setState({quanity:noofitem})}/>
            </Body>
           </View>
           <View style={{backgroundColor:'#fff',width:screenwidth,marginTop:7}}>
          <List>
            <ListItem onPress={()=>this.setState({open:true,type:"color"})}>
          <Left>
            <Text style={{color:'#000',fontSize:15}}>Select Color:</Text>
           </Left>
           <TouchableOpacity style={{height:10,width:10,paddingLeft:5,borderRadius:10,backgroundColor:this.state.color}}></TouchableOpacity>
           {/* <Text style={{fontSize:15,paddingLeft:5,color:this.state.color}}></Text> */}
           <Right><AntDesign name="caretdown" style={{ color: "#000", fontSize: 15 }} /></Right>
            </ListItem>     
            </List>
            </View>    
          </View>
           :
           <View style={{alignContent:'center',alignItems:'center'}}>
          <TouchableOpacity style={{width:screenwidth/1.5,height:50,marginTop:20,justifyContent:'center',alignItems:'center',alignContent:'center',backgroundColor:'#F53844',}}
          onPress={()=>{ this.setState({open:true,type:"item"})}}>
          <Text style={{color:'#fff',fontSize:20}}>Select Item</Text></TouchableOpacity>
          </View> }
          </Content>
          <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#42378F','#F53844']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity  style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}
            onPress={()=>this.submit()}>
                <Text style={{ color: '#fff',textAlign:'center'}}>SUBMIT</Text>
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer>
          <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
           {this.openmodal()}      
          </Modal>
          <Modal1 
            closeOnTouchOutside={false} disableOnBackPress={true}
            offset={0}
            open={this.state.open1}
            modalDidOpen={this.modalDidOpen1}
            modalDidClose={this.modalDidClose1}
            style={{ alignItems: "center", backgroundColor: "#fff", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#F53844' />
              </View>
            </Modal1>
       </Container>   
    );
  }
}
