
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TouchableOpacity,StatusBar,Dimensions,BackHandler,Linking,AsyncStorage} from 'react-native';
import {Container,Content,Spinner,Icon,Header,Left,Body,Title,Right,Grid} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import {PermissionsAndroid} from 'react-native';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import AntDesign from 'react-native-vector-icons/AntDesign';
import urldetails from '../config/endpoints.json';
import Modal from "react-native-simple-modal";
export default class Home extends Component {
  state = {
    open:false,
    employeeid:'',
    officeid:'',
    latitude:'',
    longitude:'',
    error:'',

  };

  modalDidOpen = () => console.log("Modal did open.");
    modalDidClose = () => {
      this.setState({ open: false });
    };
    openModal = () => this.setState({ open: true });
    closeModal = () => this.setState({ open: false });

  componentWillMount(){
    AsyncStorage.getItem("user")
    .then((value) => {
      const values = JSON.parse(value)
      this.setState({employeeid:values.employeeid,officeid:values.officeid })
    })
    this.requestCameraPermission();
  }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        console.log("back press")
        BackHandler.exitApp()
      };
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }

      async requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'App Camera Permission',
              message:
                'App needs access to your camera ' +
                'so you can scan Barcode.',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              return true;
            // this.props.navigation.navigate('Home');
            // Toast.show("Permission Granted. Please re-open Scan Package");
            
          } else {
            // this.props.navigation.navigate('Home');
            return false;
            
          }
        } catch (err) {
          console.warn(err);
        }
      }

      mark_attendance(){
        this.setState({open:true})
      navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log(position);
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
            });
            this.mark_now();
          },
          (error) =>
          this.geo_error(error.message),
          { enableHighAccuracy: true, timeout: 2000, maximumAge: 3600000 },
        );
    }

    geo_error(error){
      Alert.alert( 'Try Again', error,
      [ {text: 'Retry', onPress: () =>this.mark_attendance()},
      {text: 'OK', onPress: () =>console.log("cancel")}],
      { cancelable: false })
        console.log(error);
        this.setState({open:false})
    }

    mark_now(){
      let req_data = JSON.stringify({"employeeid":this.state.employeeid,"latitude":this.state.latitude,"longitude":this.state.longitude,"officeid":this.state.officeid});
      fetch(urldetails.base_url+urldetails.url["mark_attendance"],{ method: "POST",
        headers: { Accept: 'application/json', 'Content-Type':'application/json'},
        body:req_data
      }) .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        this.setState({open:false})
       alert(jsondata.msg);
      }).catch((error) => {
              this.setState({open:false})
              console.log(error)
              Alert.alert( 'Alert', 'Please check your internet connection',
                [ {text: 'Retry', onPress: () =>this.mark_now()},
                {text: 'OK', onPress: () =>console.log("cancle")}],
                { cancelable: false })
              }); 
    }


  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#F53844' }}>
        <Left/>
          <Body><Title>Raj Electronics</Title></Body>
          <Right/>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
        <Content>

          <Grid>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Selectitem")}
              style={{
                height: (Dimensions.get("window").height / 4) - 20,
                width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#b35900',
                alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
              }}>
              <Icon name="ios-add-circle-outline" style={{ fontSize: 60, color: '#fff' }} />
              <Text style={{ color: "white", fontWeight: "bold" }}>Add Stock</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate("Selectinvoice")}
              style={{
                height: (Dimensions.get("window").height / 4) - 20,
                width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#408000',
                alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
              }}>
              <Icon name="ios-remove-circle-outline" style={{ fontSize: 60, color: '#fff' }} />
              <Text style={{ color: "white", fontWeight: "bold", marginTop: 10 }}>Stock Out</Text>
            </TouchableOpacity>
          </Grid>

          <Grid>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Customerfeedback")}
              style={{
                height: (Dimensions.get("window").height / 4) - 20,
                width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#00b3b3',
                alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
              }}>
              <Icon name="stats" style={{ fontSize: 60, color: '#fff' }} />
              <Text style={{ color: "white", fontWeight: "bold" }}>Customer Feedback</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate("Deliveryconfirmation")}
              style={{
                height: (Dimensions.get("window").height / 4) - 20,
                width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#52527a',
                alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
              }}>
              <Icon name="cube" style={{ fontSize: 60, color: '#fff' }} />
              <Text style={{ color: "white", fontWeight: "bold", marginTop: 10 }}>Delivery Confirmation</Text>
            </TouchableOpacity>
          </Grid>

          <Grid>
                   <TouchableOpacity onPress={() => this.props.navigation.navigate("Emp_Attendance")}
                           style={{
                               height: (Dimensions.get("window").height / 4) - 20, 
                               width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#ff884d', 
                               alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
                           }}>
                           <AntDesign name="calendar" style={{ fontSize: 60, color:'#fff' }} />
                           <Text style={{ color: "white", fontWeight: "bold" }}>View Attendance</Text>
                       </TouchableOpacity>

                       <TouchableOpacity onPress={() => this.mark_attendance()}
                           style={{
                               height: (Dimensions.get("window").height / 4) - 20, 
                               width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#5c8a8a', 
                               alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
                           }}>
                           <Icon name="body" style={{ fontSize: 60, color:'#fff' }} />
                           <Text style={{ color: "white", fontWeight: "bold" }}>Mark My Attendance</Text>
                       </TouchableOpacity>

          </Grid>

          <Grid>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Logout")}
              style={{
                height: (Dimensions.get("window").height / 4) - 20,
                width: (Dimensions.get("window").width / 2) - 20, backgroundColor: '#862d59',
                alignItems: "center", justifyContent: "center", elevation: 8, margin: 10
              }}>
              <Icon name="power" style={{ fontSize: 60, color: '#fff' }} />
              <Text style={{ color: "white", fontWeight: "bold" }}>Logout</Text>
            </TouchableOpacity>
          </Grid>

        </Content>
        <Text style={{ fontSize: 12, color: '#595959', textAlign: 'center', marginBottom: 6 }} onPress={() => Linking.openURL('https://epsumlabs.com/')}>Powered by Epsum Labs Pvt. Ltd.</Text>
        <Modal offset={0} open={this.state.open} modalDidOpen={this.modalDidOpen} disableOnBackPress={true}
          closeOnTouchOutside={false} modalDidClose={this.modalDidClose} style={{ alignItems: "center" }}>
          <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center', marginTop: 10 }}>
            <Spinner color='#df3a45' />
            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>Please wait...</Text>
          </View>
        </Modal>

      </Container>
    );
  }
}


