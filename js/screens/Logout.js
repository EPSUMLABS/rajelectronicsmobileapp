import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage,ImageBackground,Dimensions,BackHandler } from 'react-native';
import Modal from "react-native-simple-modal";
import {Spinner, Container, Content} from 'native-base';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
class Logout extends Component {
    componentWillMount(){
        AsyncStorage.setItem('user',"");
        this.props.navigation.navigate('Redirect');
    }
    render() {
        return (
            <Container>  
             {/* <ImageBackground source={require('../assets/Sample_3.png')}  style={{flex: 1,backgroundColor:'#28384a'}} >       
            </ImageBackground>
            <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenWidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#28384a' />
              </View>
            </Modal> */}
            </Container>
        );
    }
}
//make this component available to the app
export default Logout;