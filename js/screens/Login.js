
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,Item,Label,Input,Icon, Right,Card
  } from 'native-base';
  const screenWidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;
  import Modal from "react-native-simple-modal";
  import LinearGradient from 'react-native-linear-gradient';
  import urldetails from '../config/endpoints.json';
  import ConfigureTracking from '../modules/ConfigureTracking';
  import {PermissionsAndroid} from 'react-native';

  async function requestPermissions() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'We need location permission',
          'message': 'We need to access location for the app to work.',
          buttonPositive: 'OK',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true
      } else {
        console.log('Location permission not granted');
        return false;
      }
    } catch (err) {
      console.warn(err);
    }
  }
  
  export default class Login extends Component{
    state={
      id:'',
      password:'',
      open:false
    }

    async componentWillMount(){
      granted = await requestPermissions();
    }
    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
      console.log("back press")
      BackHandler.exitApp()
    };
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
      clearInterval(this.countDown);
    }
    login(){
      //this.props.navigation.navigate("Home")
      if(this.state.userid == ""){
        alert("Please Enter UserId")
      }
      else if(this.state.password == ""){
        alert("Please Enter Password")
      }
      else{
        this.setState({open:true})
      let x = JSON.stringify({"userid":this.state.id,"password":this.state.password});
      console.log(x)
      fetch(urldetails.base_url+urldetails.url["employeelogin"],{
        method: "POST",
        headers: {
           Accept: 'application/json',
          'Content-Type':'application/json',
        },
        body:x
      })
    .then((response)=>response.json())
    .then((jsondata)=>{
      this.setState({open:false})
      console.log(jsondata)
      if(jsondata.status=="Sucess")
      {
       ConfigureTracking.configure(jsondata.employeeid, jsondata.track_start, jsondata.track_end)
        var userdetails={
          officeid:jsondata.officeid,
          userid:this.state.id,
          password:this.state.password,
          employeeid:jsondata.employeeid,
          track_start:jsondata.track_start,
          track_end:jsondata.track_end

        }
        AsyncStorage.setItem('user',JSON.stringify(userdetails));

        this.props.navigation.navigate("Home");
      }
      else{
        alert(jsondata.status)
      }
    }).catch((error) => {
      this.setState({open:false})
      console.log(error)
      this.setState({open:false})
      Alert.alert(
        'Alert',
        "Please check your Internet Connection.",
        [
          {text: 'Retry', onPress: () =>this.login()},
        ],
        { cancelable: false }
      )
      }); 
    }
  }
  render() {
    return (
         <Container>
          <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
         <LinearGradient colors={['#F53844','#42378F']} style={{ flex: 1, alignItems: 'center' }}>
         <Content>
           <View style={{alignItems: 'center', justifyContent: 'center',marginTop:120}}>
           <Text style={{fontSize:30,color:'#fff',paddingBottom:15}}>Raj Electronics</Text>
          <Card style={{width:screenWidth-40, height: screenHeight / 2, backgroundColor:'#fff', paddingTop:40, alignContent:'center', alignItems:'center', elevation:5}}>
          <Text style={{fontSize:26,color:'#F53844'}}>Login</Text>
         <View style={{flexDirection:'row',marginTop:10,}}>
         <Icon name='ios-person' style={{color:'#585858',fontSize:32,paddingTop:10,paddingRight:5,}} />
          <Item style={{width:screenWidth-110,Color:'#42378F',borderBottomColor:'#585858',borderBottomWidth:1,flexDirection:'row'}} >
          <Input placeholder="Username" placeholderTextColor="#585858" onChangeText={(id)=>this.setState({id:id})}/>
          </Item>
          </View>
            <View style={{flexDirection:'row',marginTop:10,}}>
         <Icon name='ios-lock' style={{color:'#585858',fontSize:32,paddingTop:10,paddingRight:5,}} />
          <Item style={{width:screenWidth-110,Color:'#585858',borderBottomColor:'#585858',borderBottomWidth:1,flexDirection:'row'}} >
          <Input placeholder="Password" placeholderTextColor="#585858" onChangeText={(password)=>this.setState({password: password})} secureTextEntry={true}/>
          </Item>
          </View>
          <TouchableOpacity onPress={()=>this.login()} style={{width:screenWidth/2.3,height:screenHeight/15,backgroundColor:'#df3a45',borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center',marginTop:45}}>
          <Text style={{fontSize:20,color:'#fff'}}>LOGIN</Text>
          </TouchableOpacity>
          </Card>
          {/* <View style={{flexDirection:'row',paddingTop:15}}>
        <TouchableOpacity >
        <Text style={{color:'#fff',fontSize:15}}>Create Account</Text>
        </TouchableOpacity>
        <TouchableOpacity >
        <Text style={{color:'#fff',fontSize:15,paddingLeft:'30%'}}>Forgot Password</Text>
        </TouchableOpacity>
        </View> */}
        </View>
        </Content>
        <Modal offset={0} open={this.state.open} modalDidOpen={this.modalDidOpen} disableOnBackPress={true}
          closeOnTouchOutside={false} modalDidClose={this.modalDidClose} style={{ alignItems: "center" }}>
          <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center', marginTop: 10 }}>
            <Spinner color='#df3a45' />
            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>Please wait...</Text>
          </View>
        </Modal>
        </LinearGradient>
       </Container>
    );
  }
}


