
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Picker,List,ListItem,
Card,CardItem,Radio} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import urldetails from '../config/endpoints.json';
export default class Splash extends Component{
    state={
    sellid: this.props.navigation.state.params.sellid,
    invoiceno: this.props.navigation.state.params.invoiceno,
    open:false,
    invoicelist:[],
    invoicedetails:[]  
    }
    componentWillMount(){
      this.stockinvoicedetail()
      }
       componentDidMount() {
         BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
       }
       onBackPress = () => {
         this.props.navigation.navigate("Selectinvoice")
         // BackHandler.exitApp()
       };
       componentWillUnmount() {
         BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
         clearInterval(this.countDown);
       }
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });
       
      modalDidOpen1 = () => console.log("Modal did open.");
      modalDidClose1 = () => {
        this.setState({ open1: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal1 = () => this.setState({ open1: true });
      closeModal1 = () => this.setState({ open1: false });

      stockinvoicedetail(){
        this.setState({open1:true})
        let x = JSON.stringify({"sellid":this.state.sellid});;
        console.log(x)
        fetch(urldetails.base_url+urldetails.url["stockinvoicedetails"],{
          method: "POST",
          headers: {
             Accept: 'application/json',
            'Content-Type':'application/json',
          },
          body:x
        })
      .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)  
        if(jsondata.status=="Sucess"){
          console.log(jsondata.invoice)
          this.setState({open1:false,invoicedetails:jsondata.invoice})
      }
      })  
      }
  render() {
    return (
      <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
       <Left/>
          <Body><Title>Stock Out</Title></Body>
          <Right/>
          </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content>
        
        <View style={{alignContent:'center',justifyContent:'center',alignItems:'center',marginTop:10,flexDirection:'row'}}>
        <Text style={{fontWeight:'bold',fontSize:15}}>Invoice Number : </Text>
        <Text style={{fontSize:15}}>{this.state.invoiceno}</Text>
        </View>
        <View style={{alignContent:'center',alignItems:'center'}}>
        {this.state.invoicedetails.map((item,i)=>{
        return(
         <Card style={{width:screenwidth-20}} key={i}>
          <CardItem style={{backgroundColor:'#EBEBEB'}}>
          <Text style={{ fontSize: 16 }}>Item Name : </Text>
          <Text style={{ fontSize: 16 }}>{item.itemname}</Text>
          </CardItem>
          <CardItem>
          <Text style={{ fontSize: 16 }}>Quantity : </Text>
          <Text style={{ fontSize: 16 }}>{item.quantity}</Text>
          </CardItem>
          <CardItem>
          <Text style={{ fontSize: 16 }}>Total Amount : </Text>
          <Text style={{ fontSize: 16 }}>{item.amount}</Text>
          </CardItem>
        <TouchableOpacity style={{width:screenwidth-20,height:40,padding:10,alignContent:'center',alignItems:'center',justifyContent:'center',backgroundColor:'#F53844'}}
        onPress={()=>this.props.navigation.navigate("Scanbarcode",{itemid:item.itemid,sellid:item.selldocid})}>
        <Text style={{fontSize:15,color:'#fff'}}>Scan Barcode</Text>
        </TouchableOpacity>  
          </Card>
        ) 
        })
      }
         </View>        
       
       </Content>
       {/* <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#42378F','#F53844']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity  style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}
            onPress={()=>this.setState({open:true})}>
                <Text style={{ color: '#fff',textAlign:'center'}}>SUBMIT</Text>
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer> */}
       {/* <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
         <View>
         <List dataArray={this.state.invoicelist} renderRow={(myitem) =>
            <ListItem onPress={()=>this.stockinvoicedetail(myitem.selldocid,myitem.invoiceno)} >
            <Text>{myitem.invoiceno}</Text>
            </ListItem>
                }>
             </List> 
          </View>           
          </Modal> */}
          <Modal1 
            closeOnTouchOutside={false} disableOnBackPress={true}
            offset={0}
            open={this.state.open1}
            modalDidOpen={this.modalDidOpen1}
            modalDidClose={this.modalDidClose1}
            style={{ alignItems: "center", backgroundColor: "#fff", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#F53844' />
              </View>
            </Modal1>
       </Container>
    );
  }
}
const styles = {
preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}
