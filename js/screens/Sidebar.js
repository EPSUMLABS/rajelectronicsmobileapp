import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  Platform,
  TouchableWithoutFeedback,
  Dimensions,
  AsyncStorage,
} from 'react-native';
import {
  Container,
  Content,
  Body,
  Header,
  Label,
  ListItem,
  Thumbnail,Icon
} from 'native-base';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import LinearGradient from 'react-native-linear-gradient';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
var navitems =[
  {name:'Home',nav:'Home',image:<Icon name='ios-home'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  {name:'Addstock',nav:'Selectitem',image:<Icon name='ios-add-circle-outline'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  {name:'Stockout',nav:'Selectinvoice',image:<Icon name='ios-remove-circle-outline'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  {name:'Customerfeedback',nav:'Customerfeedback',image:<Icon name='ios-star'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  {name:'Deliveryconfirmation',nav:'Deliveryconfirmation',image:<Icon name='ios-star'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  // {name:'Location',nav:'Location',image:<Icon name='ios-add'
  // style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
  {name:'Logout',nav:'Logout',image:<Icon name='ios-power'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#F53844'}}/>},
]
export default class Sidebar extends Component {
  render() {
    return (
        <Container>
          <LinearGradient colors={['#F53844','#42378F']} style={{height:160,alignItems:"center", justifyContent: 'center',backgroundColor:'#F53844',}}>
          {/* <Image source={{uri:'http://naapbooks.com/wp-content/uploads/2018/10/user1.png'}} style={{width:80, height:80}} /> */}
          <Text style={{color:'#fff', backgroundColor:'transparent', padding:4, textAlign:'center',fontSize:18}}>username</Text>
          <Text style={{color:'#fff', backgroundColor:'transparent', padding:4, textAlign:'center',fontSize:15}}>email</Text>
          </LinearGradient>
      <View style={{borderWidth:0, flex:1, backgroundColor:'white'}}>
        <Content style={{backgroundColor:'#fff'}}>
        <View>
          {navitems.map((l,i,)=>{
            return (<ListItem key={i} style={{height:50}} onPress={()=>{
                  this.props.navigation.toggleDrawer(),
                  this.props.navigation.navigate(l.nav)}}>
            <View style={{flexDirection:'row',backgroundColor:'#fff0',marginRight:50,
                              }}>
                  {l.image}
            <Text style={{fontSize:15,marginLeft:16,marginTop:16,color:'#000'}}>{l.name}</Text>
            </View></ListItem>)})
              }
        </View>
        </Content>
      </View>
      </Container>
    );
  }
}
