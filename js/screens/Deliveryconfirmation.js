
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage,Alert} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,ScrollView,List,ListItem,
    Card,CardItem} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import Modal2 from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
export default class Deliveryconfirmation extends Component {
  state={
    open:true,
    open1:false,
    open2:false,
    invoicelist:[],
    selected_selldocid:'',
    otp:''
  }
  componentWillMount(){
    this.fetch_delivery_invoice();
    }
    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
     this.props.navigation.navigate("Home")
     return true;
 
    };
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
      clearInterval(this.countDown);
    }
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });

  modalDidOpen1 = () => console.log("Modal did open.");
  modalDidClose1 = () => {
    this.setState({ open1: false });
  };
  openModal1 = () => this.setState({ open1: true });
  closeModal1 = () => this.setState({ open1: false });

  modalDidOpen2 = () => console.log("Modal did open.");
  modalDidClose2 = () => {
    this.setState({ open2: false });
  };

  openModal2 = () => this.setState({ open2: true });
  closeModal2 = () => this.setState({ open2: false });

  mark_delivered(){
    if(this.state.otp.length==5){
      this.setState({open:true})
      fetch(urldetails.base_url+urldetails.url["MarkAsDelivered"],{
        method: "POST",
        headers: {
           Accept: 'application/json',
          'Content-Type':'application/json',
        },
        body:JSON.stringify({"otp":this.state.otp,"selldocid":this.state.selected_selldocid})
      })
    .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)  
        this.setState({open:false})
        alert(jsondata.Message[0]["Column1"])
      })
    }
    else{
      alert("Invalid OTP, Please try again");
    }
    
  }


  fetch_delivery_invoice(){
    fetch(urldetails.base_url+urldetails.url["OrderToDeliver"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      }
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Success"){
      this.setState({invoicelist:jsondata.invoice,open:false, open2:true})
    }
  else{
    this.setState({open:false})
    Alert.alert(
      'Alert',
      "No order to deliver",
      [
        {text: 'ok', onPress: () =>this.props.navigation.navigate("Home")},
      ],
      { cancelable: false }
    )
  }})
  }
  render() {
    return (
     <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
       <Left/>
          <Body><Title>Delivery Confirmation</Title></Body>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content >
        <View style={{alignContent:'center',justifyContent:'center',alignItems:'center',marginTop:15}}>
        <View style={{marginTop:10}}>
          <View style={{flexDirection:'row',borderColor:'#F53844',borderWidth:1,width:screenwidth-20,height:screenHeight/16,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Input style={{fontSize:16,color:'black'}} placeholder='Enter OTP'  onChangeText={(otp)=>this.setState({otp:otp})} autoCorrect={false} />
          </View>
          <TouchableOpacity onPress={() => this.mark_delivered()} 
         style={{width:screenwidth-20,marginTop:15,height:50,borderRadius:10}}>
         <LinearGradient colors={['#F53844','#42378F']} style={{width:screenwidth-20,height:50,borderRadius:10,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
        <Text style={{fontSize:18,color:'#fff'}}>Confirm</Text>
        </LinearGradient>
         </TouchableOpacity>
         </View>
        </View>
      </Content>
      <Modal offset={0} open={this.state.open} modalDidOpen={this.modalDidOpen} modalDidClose={this.modalDidClose}
        disableOnBackPress={true} closeOnTouchOutside={false}
          style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
          <View style={{alignContent:'center',alignItems:'center'}}>
          <Spinner color='#F53844' />
          </View>         
          </Modal>

          {/* <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1} modalDidClose={this.modalDidClose1}
          style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
          <View style={{alignContent:'center',alignItems:'center'}}>
          <View style={{flexDirection:'row',marginLeft:5}}>
             <Left/>
             <Body><Text style={{fontSize:14,color:'#000'}}>Delivery Confirmation</Text></Body>
             <Right>
          <TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({ open1: false })} >
          <Text style={{ color: '#fff', fontWeight: 'bold', padding: 3, textAlign: 'center' }}>X</Text>
          </TouchableOpacity>
             </Right>
            </View>
          <Item regular style={{width:'95%',alignItems:'center',backgroundColor:'#fff',borderColor:'#F53844',borderWidth:1,borderRadius:10,marginTop:20,textAlign:'center',justifyContent:'center'}}>
          <Input style={{fontSize:16,color:'black'}} placeholder='Enter OTP'  onChangeText={(otp)=>this.setState({otp:otp})} autoCorrect={false} />
          </Item>
          <TouchableOpacity onPress={() => this.customerfeedback()} style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                    backgroundColor:'#df3a45',alignItems: "center", justifyContent: 'center',}}>
                      <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>
                    </TouchableOpacity>
          </View>         
          </Modal1> */}

          <Modal2  closeOnTouchOutside={false} disableOnBackPress={false}
            offset={0}
            open={this.state.open2}
            modalDidOpen={this.modalDidOpen2}
            modalDidClose={this.modalDidClose2}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
         <View>
           <Text style={{color:'#000'}}>Select Invoice Number</Text>
         <List dataArray={this.state.invoicelist} renderRow={(myitem) =>
            <ListItem onPress={()=>this.setState({selected_selldocid:myitem.selldocid, open2:false})} >
            <Text>{myitem.invoiceno}</Text>
            </ListItem>
                }>
             </List> 
          </View>           
          </Modal2>
     </Container>    
    );
  }
}
