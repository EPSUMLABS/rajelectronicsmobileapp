
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Picker,List,ListItem,Radio} from 'native-base';
  import Modal from "react-native-simple-modal";
  import Modal1 from "react-native-simple-modal";
  const screenwidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;
  import urldetails from '../config/endpoints.json';
export default class Splash extends Component{
    state={
        itemlist:[],
        open:false   
    }
    componentDidMount() {
      this.getitem();
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
      console.log("back press")
     this.props.navigation.navigate("Home")
     return true;
    };
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
      clearInterval(this.countDown);
    }
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });

      modalDidOpen1 = () => console.log("Modal did open.");
      modalDidClose1 = () => {
        this.setState({ open1: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal1 = () => this.setState({ open1: true });
      closeModal1 = () => this.setState({ open1: false });
    
      getitem(){
        this.setState({open1:true})
        let x = JSON.stringify({"action":"fetch"});;
        console.log(x)
        fetch(urldetails.base_url+urldetails.url["items"],{
          method: "POST",
          headers: {
             Accept: 'application/json',
            'Content-Type':'application/json',
          },
          body:x
        })
      .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)  
        if(jsondata.status=="Sucess"){
          this.setState({itemlist:jsondata.data.transport,newitemlist:jsondata.data.transport,open1:false,open:true})
          console.log(jsondata.data.transport)
        }
        else{
          (jsondata.Message)
        }
      })
      .catch((error) => {
        this.setState({open:false})
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
          [
            {text: 'Retry', onPress: () =>this.getitem()},
          ],
          { cancelable: false }
        )
        }); 
      }
      searchitem(myitem){
        const newData = this.state.itemlist.filter((mydata)=>{
            const itemData = mydata.itemname.toUpperCase()
            console.log(itemData)
            const textData = myitem.toUpperCase()
            console.log(textData)
            return itemData.indexOf(textData)>-1
          });
          
          this.setState({myitem:myitem,newitemlist:newData});
        
        console.log(this.state.newitemlist)
                      }
  render() {
    return (
      <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
       <Left/>
          <Body><Title>Add Stock</Title></Body>
          <Right/>
          </Header>
          <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content >
      <View style={{alignContent:'center',alignItems:'center',marginTop:'40%'}}>
          <TouchableOpacity style={{width:screenwidth/1.5,height:50,marginTop:40,justifyContent:'center',alignItems:'center',alignContent:'center',backgroundColor:'#F53844',}}
          onPress={ ()=>this.getitem()}>
          <Text style={{color:'#fff',fontSize:20}}>Select Item</Text></TouchableOpacity>
          </View>
       </Content>
       <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
          
            <View style={{ flexDirection: 'column',alignContent: 'center', justifyContent: 'center' }}>
            <Item regular style={{height:40,width:screenwidth-100,alignItems:'center',borderRadius:5,marginLeft:5,borderColor: '#000'}}>
          <Input style={{fontSize:16,color:'#000'}} placeholder='Search'placeholderTextColor='#000' onChangeText={(itemname)=>this.searchitem(itemname)} autoCapitalize="none" autoCorrect={false} />
          </Item>
            <List dataArray={this.state.newitemlist} renderRow={(myitem) => 
            <ListItem onPress={()=>this.props.navigation.navigate("Addstock",{itemlist:this.state.itemlist,itemname:myitem.itemname,barcode:''})}>
            <Text>{myitem.itemname}</Text>
            </ListItem>
                          }>
             </List>
            </View>
          </Modal>
          <Modal1 
            closeOnTouchOutside={false} disableOnBackPress={true}
            offset={0}
            open={this.state.open1}
            modalDidOpen={this.modalDidOpen1}
            modalDidClose={this.modalDidClose1}
            style={{ alignItems: "center", backgroundColor: "#fff", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#F53844' />
              </View>
            </Modal1>
       </Container>
    );
  }
}


