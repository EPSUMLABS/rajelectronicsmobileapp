
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableOpacity,StatusBar,BackHandler,AsyncStorage} from 'react-native';
import {Container,Content,Spinner,Footer,FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Picker,List,ListItem,
Card,CardItem,Radio} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import urldetails from '../config/endpoints.json';
export default class Splash extends Component{
    state={
    open:false,
    sellid:'',invoiceno:'',
    invoicelist:[],
    }
    componentWillMount(){
      this.getinvoicelist()
      }
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home")
      };
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });

      modalDidOpen1 = () => console.log("Modal did open.");
      modalDidClose1 = () => {
        this.setState({ open1: false });
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal1 = () => this.setState({ open1: true });
      closeModal1 = () => this.setState({ open1: false });
      getinvoicelist(){
        this.setState({open1:true})
     let x = JSON.stringify({"action":"fetch"});;
    console.log(x)
    fetch(urldetails.base_url+urldetails.url["getinvoicelist"],{
      method: "POST",
      headers: {
         Accept: 'application/json',
        'Content-Type':'application/json',
      },
      body:x
    })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)  
    if(jsondata.status=="Sucess"){
      this.setState({open1:false})
      console.log(jsondata)
      arr=[]
      jsondata.invoice.map((myitem, i) => {
        console.log(myitem.visibility)
        if (myitem.visibility==true) {
          arr.push(myitem)
          console.log(arr)
          this.setState({invoicelist:arr,open:true})
          console.log(this.state.invoicelist)
        }
      })
    // this.setState({invoicelist:jsondata.invoice,open:true})
  }
  else{
    this.setState({open1:false})
  }
  })
      }
 
  render() {
    return (
      <Container>
       <Header style={{ backgroundColor: '#F53844' }}>
       <Left/>
          <Body><Title>Stock Out</Title></Body>
          <Right/>
          </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
      <Content>
        <View style={{alignContent:'center',justifyContent:'center',alignItems:'center',marginTop:10,flexDirection:'row'}}>
         </View>       
       </Content>
       <Modal  closeOnTouchOutside={false} disableOnBackPress={false}
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center",alignContent:'center', color: "#F5F5F5", width:screenwidth }}>
         <View>
           <Text style={{color:'#000'}}>Select Invoice Number</Text>
         <List dataArray={this.state.invoicelist} renderRow={(myitem) =>
            <ListItem onPress={()=>this.props.navigation.navigate("Stockout",{sellid:myitem.selldocid,invoiceno:myitem.invoiceno})} >
            <Text>{myitem.invoiceno}</Text>
            </ListItem>
                }>
             </List> 
          </View>           
          </Modal>
          <Modal1 
            closeOnTouchOutside={false} disableOnBackPress={true}
            offset={0}
            open={this.state.open1}
            modalDidOpen={this.modalDidOpen1}
            modalDidClose={this.modalDidClose1}
            style={{ alignItems: "center", backgroundColor: "#fff", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#F53844' />
              </View>
            </Modal1>
       </Container>
    );
  }
}
const styles = {
preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}
