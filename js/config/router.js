import {DrawerNavigator,StackNavigator} from 'react-navigation';
import React, { Component } from 'react';
import { Platform,Dimensions } from 'react-native';
import Splash from '../screens/Splash';
import Login from '../screens/Login';
import Home from '../screens/Home';
import Barcodereadstockin from '../screens/Barcodereadstockin';
//import Location from '../screens/Location';
import Addstock from '../screens/Addstock';
import Selectinvoice from '../screens/Selectinvoice';
import Stockout from '../screens/Stockout';
import Scanbarcode from '../screens/Scanbarcode';
import Selectitem from '../screens/Selectitem';
import Customerfeedback from '../screens/Customerfeedback';
import Feedbackform from '../screens/Feedbackform';
import Deliveryconfirmation from '../screens/Deliveryconfirmation';
import Logout from '../screens/Logout';
import Sidebar from '../screens/Sidebar';
import Redirect from '../screens/Redirect';
import Emp_Attendance from '../screens/Emp_Attendance';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const Drawer = ({
  Home:{screen:Home},
  Barcodereadstockin:{screen:Barcodereadstockin},
  Selectitem:{screen:Selectitem},
  Addstock:{screen:Addstock},
  Selectinvoice:{screen:Selectinvoice},
  Stockout:{screen:Stockout},
  Scanbarcode:{screen:Scanbarcode},
  Customerfeedback:{screen:Customerfeedback},
  Feedbackform:{screen:Feedbackform},
  Deliveryconfirmation:{screen:Deliveryconfirmation},
  Logout:{screen:Logout},
});
const NavigationMenu = DrawerNavigator(
  Drawer,
   {
    initialRouteName:'Home',
    drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
    contentComponent: props => <Sidebar {...props} routes={Drawer}/>,
    drawerPosition:'left',
  });
  const StackNav=StackNavigator(
    {
      Splash:{screen:Splash},
      Login:{screen:Login},
      Redirect:{screen:Redirect},
      Home:{screen:Home},
      Barcodereadstockin:{screen:Barcodereadstockin},
      Selectitem:{screen:Selectitem},
      Addstock:{screen:Addstock},
      Selectinvoice:{screen:Selectinvoice},
      Stockout:{screen:Stockout},
      Scanbarcode:{screen:Scanbarcode},
      Emp_Attendance:{screen:Emp_Attendance},
      Customerfeedback:{screen:Customerfeedback},
      Feedbackform:{screen:Feedbackform},
      Deliveryconfirmation:{screen:Deliveryconfirmation},
      Logout:{screen:Logout},
      //Location:{screen:Location},
      //NavigationMenu:{screen:NavigationMenu}
    },
  {headerMode:'none'});
  export default StackNav;
